#!/bin/sh

STAGED_FILES=$(git diff --cached --name-only --diff-filter=ACM | grep ".php\{0,1\}$")

if [[ "$STAGED_FILES" = "" ]]; then
  exit 0
fi


echo "Validating PHP_CS_Fixer:"

# Check for phpcs
which ./vendor/bin/php-cs-fixer &> /dev/null
if [[ "$?" == 1 ]]; then
  echo "Please install php-cs-fixer"
  exit 1
fi

for FILE in $STAGED_FILES
do

echo "Running PHP_CS_Fixer For: $FILE"
  ./vendor/bin/php-cs-fixer fix "$FILE"

	if [ $? != 0 ]
	then
		echo "Coding standards errors have been detected PHP_CS_Fixer..."
	fi
	git add $FILE
done

echo "PHP_CS_Fixer validation completed!"

exit $?

