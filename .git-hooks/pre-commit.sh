#!/bin/sh

sh .git-hooks/php-cs-fixer.sh || exit
sh .git-hooks/codesniffer.sh || exit
# sh git-hooks/larastan.sh || exit
sh .git-hooks/phpunit.sh || exit
