#!/bin/sh

STAGED_FILES=$(git diff --cached --name-only --diff-filter=ACM | grep ".php\{0,1\}$")

if [[ "$STAGED_FILES" = "" ]]; then
  exit 0
fi

PASS=true

echo "\nValidating PHPCS:\n"

# Check for phpcs
which ./vendor/bin/phpcs &> /dev/null
if [[ "$?" == 1 ]]; then
  echo "\t\033[41mPlease install PHPCS\033[0m"
  exit 1
fi

RULESET=./phpcs.xml

for FILE in $STAGED_FILES
do

echo "Running Code Sniffer For: $FILE"
	  ./vendor/bin/phpcs --standard="$RULESET" "$FILE"

	if [ $? != 0 ]
	then
		echo "Coding standards errors have been detected. Running phpcbf..."
		  ./vendor/bin/phpcbf --standard="$RULESET" "$FILE"

		git add $FILE
		echo "Running Code Sniffer again For: $FILE"
		  ./vendor/bin/phpcs --standard="$RULESET" "$FILE"

		if [ $? != 0 ]
		then
			echo "Errors found not fixable automatically"
			exit 1
		fi
	fi
done

echo "\nPHPCS validation completed!\n"

if ! $PASS; then
  echo "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass PHPCS but do not. Please fix the PHPCS errors and try again.\n"
  exit 1
else
  echo "\033[42mSniffer\033[0m\n"
fi

exit $?
